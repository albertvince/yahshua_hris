// import React from 'react';
// import Home from '../Yahshua_HRIS_App/app/screens/Home';
// import Login from '../Yahshua_HRIS_App/app/screens/Login';
// import JobPostings from '../Yahshua_HRIS_App/app/screens/JobPostings';
// import SaveSearches from '../Yahshua_HRIS_App/app/screens/SaveSearches';
// import AppliedJobs from '../Yahshua_HRIS_App/app/screens/AppliedJobs';
// import Profile from '../Yahshua_HRIS_App/app/screens/Profile';
// import Logout from '../Yahshua_HRIS_App/app/screens/Logout';
// import SignUp from '../Yahshua_HRIS_App/app/screens/SignUp';
// import { AppRegistry, View,Text,StyleSheet, ScrollView, Image} from 'react-native';
//   import { Container, Content, Icon, Header, Body } from 'native-base'
// import { createDrawerNavigator, DrawerItems } from 'react-navigation';

// export default class App extends React.Component {
//   render() {
//     return (
//       <NavigationDrawer />
//     );
//   }
// }

// const CustomDrawerContentComponent = (props) =>(

//     <Container>
//         <Header style={styles.drawerHeader}>
//           <Body>
//             <Image
//               style={styles.drawerImage}
//               source={require('../Yahshua_HRIS_App/app/img/yahshualogo.png')} />
//           </Body>
//         </Header>
//         <Content>
//           <DrawerItems {...props} />
//         </Content>

//       </Container>
//   )

// const NavigationDrawer = createDrawerNavigator({

//   Home: {screen: Home},
//   Jobs: {screen: JobPostings},
//   Searches: {screen: SaveSearches},
//   Applied: {screen: AppliedJobs},
//   Profile: {screen: Profile},
//   Logout: {screen: Logout},
//   Login: { screen: Login},
//   SignUp: {screen: SignUp},
// },
// {
//   initialRouteName:'Home',
//   drawerPosition:'left',
//   contentComponent:CustomDrawerContentComponent,
//   drawerOpenRoute:'DraweOpen',
//   drawerCloseRoute:'DrawerClose',
//   drawerToggleRoute:'DrawerToggle'
// });

// styles = StyleSheet.create({

//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center'
//   },
//   drawerHeader: {
//     height: 200,
//     backgroundColor: 'white'
//   },
//   drawerImage: {
//     flex: 1,
//     justifyContent: 'center',
//     height: 100,
//     width: 250,
//     alignItems: 'center'

//   }

// });

// AppRegistry.registerComponent('App', () => App);

import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation'
import Login from './app/screens/Login';
import SignUpApplicant from './app/screens/SignUpApplicant';
import Company from './app/screens/Company';
import DrawerNavigator from './app/screens/DrawerNavigator';

export default class App extends React.Component {
  render() {
    return (
      <AppStackNavigator />
    );
  }
}

const AppStackNavigator = new createStackNavigator({
  Login: { screen: Login },
  SignUpApplicant: { screen: SignUpApplicant },
  Company: { screen: Company},
  DrawerNavigator: {
    screen: DrawerNavigator,
  }
});

