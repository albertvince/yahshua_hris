import React, {Component} from 'react';
import Home from './Home';
import JobPostings from './JobPostings';
import SaveSearches from './SaveSearches';
import AppliedJobs from './AppliedJobs';
import Profile from './Profile';
import Logout from './Logout';
import { createDrawerNavigator, DrawerItems } from 'react-navigation';
import { Container, Content, Icon, Header, Body } from 'native-base';
import { AppRegistry, Image, StyleSheet} from 'react-native';

export default class DrawerNavigator extends Component {

  static navigationOptions = {
		header: null,
	}

    render() {
      return (
        <AppDrawerNavigator />
      );
    }
  }
  const CustomDrawerContentComponent = (props) =>(

        <Container>
            <Header style={styles.drawerHeader}>
              <Body>
                <Image
                  style={styles.drawerImage}
                  source={require('../img/yahshualogo.png')} />
              </Body>
            </Header>
            <Content>
              <DrawerItems {...props} />
            </Content>
    
          </Container>
      )

const AppDrawerNavigator = new createDrawerNavigator({
    Home: { screen: Home },
    Job: { screen: JobPostings},
    Searche: {screen: SaveSearches},
    Applied: {screen: AppliedJobs},
    Profile: {screen: Profile},
    Logout: {screen: Logout}
},
{
  initialRouteName:'Home',
  drawerPosition:'left',
  contentComponent:CustomDrawerContentComponent,
  drawerOpenRoute:'DraweOpen',
  drawerCloseRoute:'DrawerClose',
  drawerToggleRoute:'DrawerToggle'
});

styles = StyleSheet.create({

    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    drawerHeader: {
      height: 200,
      backgroundColor: 'white'
    },
    drawerImage: {
      flex: 1,
      justifyContent: 'center',
      height: 100,
      width: 250,
      alignItems: 'center'
  
    }
  
  });

AppRegistry.registerComponent('App', () => App);