import React, { Component } from 'react';
import { View, Text,AppRegistry,TextInput, StyleSheet,Image, YellowBox} from 'react-native';
import { Button } from 'react-native-elements';

export default class SignUpApplicant extends Component {

	static navigationOptions = {
		header: null,
	  }
	  
    render() {
		return(
			<View style={styles.container}>
						{/* <Image
							style={styles.logo}
							source={require('../img/job-icon.png')} 
							/> */}
						<Text style={styles.title}> Sign Up </Text>   

                <View style={styles.inputBox}>
						<Image source={require('../img/employees.png')} style={styles.ImageStyle} />
						<TextInput 
						style={{flex:1}}
						placeholder="Firstname"
						underlineColorAndroid="rgba(0,0,0,0)"
						placeholderTextColor="black" 
						returnKeyType="next"
						onChangeText={(username) => this.setState({username: username})}/>
				</View>  


				 <View style={styles.inputBox}>
						<Image source={require('../img/employees.png')} style={styles.ImageStyle} />
						<TextInput 
						style={{flex:1}}
						placeholder="Middle name"
						underlineColorAndroid="rgba(0,0,0,0)"
						placeholderTextColor="black" 
						returnKeyType="next"
						onChangeText={(username) => this.setState({username: username})}/>
				</View>  

                 <View style={styles.inputBox}>
						<Image source={require('../img/employees.png')} style={styles.ImageStyle} />
						<TextInput 
						style={{flex:1}}
						placeholder="Lastname"
						underlineColorAndroid="rgba(0,0,0,0)"
						placeholderTextColor="black" 
						returnKeyType="next"
						onChangeText={(username) => this.setState({username: username})}/>
				</View>        
					
				<View style={styles.inputBox}>
						<Image source={require('../img/email.png')} style={styles.ImageStyle} />
						<TextInput 
						style={{flex:1}}
						keyboardType='email-address'
						placeholder="Email"
						underlineColorAndroid="rgba(0,0,0,0)"
						placeholderTextColor="black" 
						returnKeyType="next"
						onChangeText={(username) => this.setState({username: username})}/>
				</View>

				<View style={styles.inputBox}>
					<Image source={require('../img/padlock.png')} style={styles.ImageStyle} />
					<TextInput
						secureTextEntry={true}
						style={{flex:1}}
						placeholder="Password"
						underlineColorAndroid="rgba(0,0,0,0)"
						placeholderTextColor="black" 
						returnKeyType="go"
						onChangeText={(password) => this.setState({password: password})}
						 />

				</View>

                <View style={styles.inputBox}>
					<Image source={require('../img/padlock.png')} style={styles.ImageStyle} />
					<TextInput
						secureTextEntry={true}
						style={{flex:1}}
						placeholder="Confirm Password"
						underlineColorAndroid="rgba(0,0,0,0)"
						placeholderTextColor="black" 
						returnKeyType="go"
						onChangeText={(password) => this.setState({password: password})}
						 />

				</View>

				<View>
					<Button buttonStyle={styles.buttonRegister} title="Register" textStyle={{ color: "#FFF", fontSize: 20 }}
                    />
				</View>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container : {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#1D8348'
	},

	buttonRegister: {
		width: 300,
		paddingVertical :12,
		marginVertical: 10,
		backgroundColor: '#2E86C1',
	},

	inputBox: {
		width:300,
		backgroundColor: '#FFF',
		paddingHorizontal:16,
		marginVertical: 7,	
		flexDirection: 'row',
		height: 45,
        margin: 15,
	},

	title:{
		color: 'white',
		marginTop:7,
		fontWeight: 'bold',
		textAlign:'center',
		marginVertical: 50,
		fontSize: 25
    },

    textSignup:{
		color: '#FFF',
		marginTop:7,
		textAlign:'center',
		marginVertical: 50,
        fontSize: 17,
        textDecorationLine:'underline',
    },

    textOr:{
		color: '#FFF',
		marginTop:7,
		textAlign:'center',
        fontSize: 18,
	},

	logo: {
		width:170,
		height:130,
	},

	ImageStyle: {
		padding: 10,
		marginVertical:15,
		height: 15,
		width: 15,
		resizeMode : 'stretch',
		alignItems: 'center'
	}

});

AppRegistry.registerComponent('SignUpApplicant', () => SignUpApplicant);