import React, { Component } from 'react';
import { View, Text, AppRegistry, Image, StyleSheet} from 'react-native';
import { Header } from 'react-native-elements';
import { Icon, Button, Container, Content, Left, Body, Title, Right } from 'native-base'
import CustomHeader from '../Components/CustomerHeader';

export default class Home extends Component {

    static navigationOptions = ({ navigation }) => ({
        title: "Home",
        headerLeft: <Icon name="ios-menu" style={{ paddingLeft: 10 }} onPress={() => navigation.navigate('DrawerOpen')} />,
        drawerLabel: 'Home',
        drawerIcon: ({ tintColor }) => (
          <Image
            source={require('../img/home.png')}
            style={styles.icon}
          />
        ),
      })
    

    render() {
        return(
            <Container>

                <CustomHeader title="Home" drawerOpen={() => this.props.navigation.navigate('DrawerOpen')} />
    
            <Content
              contentContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: 10 }}>
              <Button
                onPress={() => this.props.navigation.navigate('Login')} full>
                <Text style={{ color: 'white' }}>Go To Login Screen</Text>
              </Button>
            </Content>
    
          </Container> 
        );
    }
}

const styles = StyleSheet.create({
    icon: {
      width: 24,
      height: 24,
    },
  });

AppRegistry.registerComponent('Home', () => Home);