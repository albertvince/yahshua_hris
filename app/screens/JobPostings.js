import React, { Component } from 'react';
import { View, Text, AppRegistry, Image, StyleSheet} from 'react-native';
import { Header } from 'react-native-elements';
import { Icon, Button, Container, Content, Left, Body, Title, Right } from 'native-base'
import CustomHeader from '../Components/CustomerHeader';

export default class JobPostings extends Component {

    static navigationOptions = ({ navigation }) => ({
        title: "Job Posting",
        headerLeft: <Icon name="ios-menu" style={{ paddingLeft: 10 }} onPress={() => navigation.navigate('DrawerOpen')} />,
        drawerLabel: 'Job',
        drawerIcon: ({ tintColor }) => (
          <Image
            source={require('../img/job.png')}
            style={styles.icon}
          />
        ),
      })
    

    render() {
        return(
            <Container>

                <CustomHeader title="Job Posting" drawerOpen={() => this.props.navigation.navigate('DrawerOpen')} />
    
            <Content
              contentContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: 10 }}>
              <Button
                onPress={() => this.props.navigation.navigate('Profile')} full>
                <Text style={{ color: 'white' }}>Go To Profile Screen</Text>
              </Button>
            </Content>
    
          </Container> 
        );
    }
}

const styles = StyleSheet.create({
    icon: {
      width: 24,
      height: 24,
    },
  });

AppRegistry.registerComponent('JobPostings', () => JobPostings);